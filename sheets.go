package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"bitbucket.org/auth/logger"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
)

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config, log logger.Logger) *http.Client {
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config, log)
		saveToken(tokFile, tok, log)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config, log logger.Logger) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Откройте ссылку в браузере, получите разрешение и верните код "+
		"код авторизации: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatal().Err(err).Msg("Неверный код авторизации")
	}

	tok, err := config.Exchange(oauth2.NoContext, authCode)
	if err != nil {
		log.Fatal().Err(err).Msg("Не найден токен:")
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token, log logger.Logger) {

	log.Info().Str("Path ", path).Msg("Saving credential file")
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	defer f.Close()
	if err != nil {
		log.Fatal().Err(err).Msg("Unable to cache oauth token:")
	}
	json.NewEncoder(f).Encode(token)
}

//InitSheet - Initializing work with Google Sheets API
func (c *Config) InitSheet() (*sheets.Service, error) {

	b, err := ioutil.ReadFile("client_secret.json")
	if err != nil {
		c.Log.Fatal().Err(err).Msg("Unable to read client secret file")

	}

	config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		c.Log.Fatal().Err(err).Msg("Unable to parse client secret file to config")
	}
	client := getClient(config, c.Log)

	srv, err := sheets.New(client)
	if err != nil {
		c.Log.Fatal().Err(err).Msg("Unable to retrieve Sheets Client")
		log.Fatalf("Unable to retrieve Sheets Client %v", err)
	}
	return srv, err
}

//SheetsAppend - Publish data to a table
func (c *Config) SheetsAppend(srv *sheets.Service, Out Answer) {
	ctx, cancel := context.WithTimeout(context.Background(), CtxTimeOut*time.Second)
	writeRange := "A1"
	var vr sheets.ValueRange
	var err error

	myval := []interface{}{Out.Time.Format("02.01.2006"), Out.Time.Format("15:04:05"), Out.Course}
	vr.Values = append(vr.Values, myval)
	_, err = srv.Spreadsheets.Values.Append(c.Sheet.SpreadsheetId, writeRange, &vr).ValueInputOption("RAW").Context(ctx).Do()
	if err != nil {
		cancel()
		c.Log.Error().Err(err).Msg("Ошибка записи в таблицу")
	}
	cancel()
}
